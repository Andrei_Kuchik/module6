﻿using System;
using System.Reflection;
using MyIoC;

namespace ConsoleApplication
{
	internal class Program
	{
		private static void Main(string[] args)
		{
			var container = new Container(Assembly.LoadFrom("MyIoC.dll"));
			var customerBLL = container.CreateInstance(typeof(CustomerBLL));
			var customerBLL2 = container.CreateInstance(typeof(CustomerBLL2));
			var first = container.CreateInstance<Test1>();
			Console.WriteLine(first.test2);
			Console.ReadLine();
		}
	}
}